package statement

// Statement contains a transaction info
type Statement struct {
	ID              string `json:"id"`              // Unique id of transaction
	Time            int    `json:"time"`            // Transaction time in seconds in Unix time format
	Description     string `json:"description"`     // Transaction description
	MCC             int    `json:"mcc"`             // Merchant Category Code, ISO 18245
	Hold            bool   `json:"hold"`            // Amount lock status
	Amount          int    `json:"amount"`          // Amount in the currency of the account in the minimum units of currency
	OperationAmount int    `json:"operationAmount"` // Amount in the currency of the transaction currency in the minimum units of currency
	CurrencyCode    int    `json:"currencyCode"`    // The currency code of the account is in accordance with ISO 4217
	CommissionRate  int    `json:"commissionRate"`  // The size of the commission in the minimum units of currency
	CashbackAmount  int    `json:"cashbackAmount"`  // The size of the cashback in the minimum units of currency
	Balance         int    `json:"balance"`         // Account balance
}

// Statements represents a list of transactions at the specified time
type Statements []Statement
