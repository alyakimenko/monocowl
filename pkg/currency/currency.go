package currency

// Currency represents a basic list of currency mono-bank rates
type Currency struct {
	CurrencyCodeA int     `json:"currencyCodeA"` // The currency code of the account is in accordance with ISO 4217
	CurrencyCodeB int     `json:"currencyCodeB"`
	Date          int64   `json:"date"` // Course time in seconds Unix time format
	RateBuy       float32 `json:"rateBuy"`
	RateSell      float32 `json:"rateSell"`
}
