package client

type Client struct {
	Name       string    `json:"name"`       // Client name
	WebHookURL string    `json:"webHookUrl"` // URL to get information about the new transaction
	Accounts   []Account // List of available accounts
}

type Account struct {
	ID           string `json:"id"`           // Account ID
	Balance      int    `json:"balance"`      // Account balance in the minimum units of currency (pennies, cents)
	CreditLimit  int    `json:"creditLimit"`  // Credit limit
	CurrencyCode int    `json:"currencyCode"` // The currency code of the account is in accordance with ISO 4217
	CashbackType string `json:"cashbackType"` // The type of cache that is charged to the account
}
