package util

import "time"

// ParseUnixTimestamp returns more humane course timestamp
func ParseUnixTimestamp(unixTime int64, layout string) string {
	return time.Unix(unixTime, 0).Format(layout)
}
