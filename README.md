# monocowl

Self-hosted monobank web client.

### Preparation

To access the monobank public API you'll need a token that can be obtained via [this link](https://api.monobank.ua/).

After that, you should set the environment variable to store the token.

```bash
export MONO_API_TOKEN=
```

Your actual token goes after equal sign.