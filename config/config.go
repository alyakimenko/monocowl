package config

type configuration struct {
	StaticDir string
}

var Config = configuration{
	StaticDir: "../web/",
}
